# URL Shortener

A URL shortener website with simple filterable report that tracks the number of clicks, originating geolocation and timestamp of each visited shortened urls

### Prerequisite

- Docker (25.0.3)
- Docker Compose (v2.24.6-desktop.1)
- gcloud (Only required for deployment)

### Tech Stack

- Rails (7.1.3.2)
- Ruby (3.3.0)
- Postgres (15.5)
- GCP

### Installation

Navigate to the app folder in terminal and run the following commands in sequence.

```sh
cd url-shortener
```

Spin up the stack by running the following command

```sh
docker-compose up -d
```

Use the following command to observe and ensure the web and db services are in healthy state

```sh
docker ps
```

To shut down the services

```sh
docker compose down
```

You may open the website via [localhost:8080](localhost:8080)

### Development

To enable rails live reload, navigate to the web folder and run the following command

```sh
./bin/bundle exec guard
```

To deploy a migration

```sh
docker exec -it url-shortener-web-1 /bin/bash -c "bin/rails db:migrate"
```

To rollback a migration

```sh
docker exec -it url-shortener-web-1 /bin/bash -c "bin/rails db:rollback"
```

### Manage packages

Ensure to rebuild the docker image after adding a gem to the Gemfile, following command will restart the app in clean state

```sh
docker compose down --rmi local && docker compose up
```

### Clean up

The following command removes the image and volume

```sh
docker compose down --rmi local -v
```

### Deployment

To build and deploy the app, navigate to web folder and run the following in sequence:

```sh
gcloud builds submit --config cloudbuild.yaml \
    --substitutions _SERVICE_NAME=url-shortener
```

Fill up the placeholders and run the following command:

```sh
gcloud run deploy url-shortener \
     --platform managed \
     --region asia-southeast1 \
     --image gcr.io/synthetic-trail-418815/url-shortener \
     --add-cloudsql-instances <CONNECTION NAME> \
     --set-env-vars="POSTGRES_DB_NAME=<POSTGRES DB NAME>" \
     --set-env-vars="POSTGRES_USER=<POSTGRES USER>" \
     --set-env-vars="POSTGRES_HOST=<POSTGRES HOST>" \
     --set-env-vars="POSTGRES_PASSWORD=<POSTGRES PASSWORD>" \
     --set-env-vars="GOOGLE_PROJECT_ID=<GOOGLE PROJECT ID>" \
     --set-env-vars="RAILS_MASTER_KEY=<RAILS MASTER KEY>"
```

### Visit the website

Here's the live website [link](https://url-shortener-2kc3lwmaaq-as.a.run.app/)
