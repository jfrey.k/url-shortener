class AddTitleTagToTargetUrls < ActiveRecord::Migration[7.1]
  def change
    add_column :target_urls, :title_tag, :string
  end
end
