class CreateTargetUrls < ActiveRecord::Migration[7.1]
  def change
    create_table :target_urls do |t|
      t.text :url, null: false
      t.timestamps
    end

    add_index :target_urls, :url, unique: true
  end
end
