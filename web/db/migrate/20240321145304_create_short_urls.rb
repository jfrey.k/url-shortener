class CreateShortUrls < ActiveRecord::Migration[7.1]
  def change
    create_table :short_urls do |t|
      t.string :path, null: false
      t.references :target_url, null: false, foreign_key: { on_delete: :cascade }
      t.timestamps
    end

    add_index :short_urls, :path, unique: true
  end
end
