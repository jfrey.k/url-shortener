class CreateGeolocations < ActiveRecord::Migration[7.1]
  def change
    create_table :geolocations do |t|
      t.float :latitude
      t.float :longitude
      t.references :short_url, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
