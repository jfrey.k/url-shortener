# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

require "faker"

["http://www.thelongestdomainnameintheworldandthensomeandthensomemoreandmore.com/", "https://www.helloworld.org/",
 "https://www.isitdarkoutside.com/", "https://www.howmanypeopleareinspacerightnow.com/", "http://www.ismycomputeron.com/", "http://www.koalastothemax.com/", "https://www.leduchamp.com/"].each do |url|
  target_url = TargetUrlService.new(url).call

  short_url = ShortUrlService.new(target_url).call

  rand(30..100).times do
    Geolocation.create(latitude: Faker::Address.latitude, longitude: Faker::Address.longitude, short_url: short_url,
                       created_at: Faker::Time.between(from: DateTime.now - 10, to: DateTime.now))
  end
end
