class TargetUrlService
  def initialize(url)
    unless URI.parse(url).is_a?(URI::HTTP)
      url = "http://#{url}"
    end
    
    @target_url = TargetUrl.find_or_initialize_by(url: url)
  end

  def call
    return @target_url unless @target_url.valid?

    begin
      page_content = Mechanize.new.get(@target_url.url)
    rescue StandardError => e
      # Log error most likely due to invalid URL
      puts "Mechanize error #{e.inspect}"
      page_content = nil
    end
    @target_url.title_tag = page_content.nil? ? nil : page_content.title
    @target_url.save!
    @target_url
  end
end
