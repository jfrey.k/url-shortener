class VisitorGeolocationService
  def initialize(visitor_ip, short_url)
    @visitor_ip = visitor_ip
    @short_url = short_url
  end

  def call
    geodata = Geocoder.search(@visitor_ip)
    if geodata.empty? || geodata.first.coordinates.empty?
      latitude, longitude = [nil, nil]
    else
      latitude, longitude = geodata.first.coordinates
    end

    Geolocation.create(latitude: latitude, longitude: longitude, short_url: @short_url)
  end
end
