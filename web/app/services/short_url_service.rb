class ShortUrlService
  def initialize(target_url)
    @target_url = target_url
  end

  def call
    path = SecureRandom.alphanumeric(ShortUrl.path_limit)
    @short_url = ShortUrl.new(path: path, target_url: @target_url)
    @short_url.save!
    @short_url
  end
end
