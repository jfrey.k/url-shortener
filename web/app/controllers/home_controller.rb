class HomeController < ApplicationController
  def index
    if session[:short_url_id]
      @has_session = true
      @short_url = ShortUrl.find(session[:short_url_id]).decorate(context: { domain: request.original_url })
      @target_url = @short_url.target_url
      session.delete :short_url_id
      return
    end

    @has_session = false
    @target_url = TargetUrl.new
  end

  def create
    begin
      @target_url = TargetUrlService.new(target_url_params[:url]).call
      unless @target_url.valid?
        render :index, status: :unprocessable_entity
        return
      end

      short_url = ShortUrlService.new(@target_url).call
      unless short_url.valid?
        render :index, status: :unprocessable_entity
        return
      end
    rescue StandardError => e
      puts "error #{e.inspect}"
      render :index, status: :unprocessable_entity
      return
    end

    session[:short_url_id] = short_url.id
    redirect_to action: 'index'
  end

  private

  def target_url_params
    params.require(:target_url).permit(:url)
  end
end
