class RedirectController < ApplicationController
  def redirect_url
    short_url = ShortUrl.find_by(path: params[:path])

    if short_url.nil?
      render file: "#{Rails.root}/public/404.html", status: :not_found
      return
    end

    VisitorGeolocationService.new(request.remote_ip, short_url).call

    redirect_to short_url.target_url.url, allow_other_host: true
  end
end
