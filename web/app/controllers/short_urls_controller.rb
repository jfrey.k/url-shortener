class ShortUrlsController < ApplicationController
  def index
    flash.clear

    @path = filter_params[:path] || ""
    @start_date = filter_params[:start_date] || ""
    @end_date = filter_params[:end_date] || ""

    list_result_query = ShortUrlGeoQuery.new(path: @path, start_date: @start_date, end_date: @end_date,
                                             select: "short_urls.path, geolocations.latitude, geolocations.longitude, geolocations.created_at", order_by: "geolocations.created_at DESC")

    unless list_result_query.valid?
      flash[:errors] = list_result_query
      render :index, status: :unprocessable_entity
      return
    end

    @pagy, @records = pagy(list_result_query.call)

    aggregated_result_query = ShortUrlGeoQuery.new(path: @path, start_date: @start_date, end_date: @end_date,
                                                   select: "short_urls.path, target_url_id, COUNT(short_urls.path) as num_of_clicks", group_by: "short_urls.path, target_url_id", order_by: "COUNT(short_urls.path) DESC")


    unless aggregated_result_query.valid?
      flash[:errors] = aggregated_result_query
      render :index, status: :unprocessable_entity
      return
    end

    @aggregated_result = aggregated_result_query.call
  end

  private

  def filter_params
    params.fetch(:filters, {}).permit(:path, :start_date, :end_date)
  end
end
