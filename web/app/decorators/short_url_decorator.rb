class ShortUrlDecorator < ApplicationDecorator
  delegate_all

  def short_url_link
    context[:domain] + object.path
  end

  # Following method has side effect with pagy pagination
  # def formatted_created_at
  #   object.created_at.to_formatted_s(:db)
  # end
end
