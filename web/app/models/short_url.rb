class ShortUrl < ApplicationRecord
  @@path_limit = 15

  validates :path, length: { maximum: @@path_limit }, uniqueness: true, presence: true

  belongs_to :target_url
  has_many :geolocations, dependent: :destroy

  def self.path_limit
    @@path_limit
  end
end
