class TargetUrl < ApplicationRecord
  validates :url, uniqueness: true, presence: true, format: { with: URI::DEFAULT_PARSER.make_regexp }

  has_many :short_urls, dependent: :destroy
end
