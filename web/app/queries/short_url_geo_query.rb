class ShortUrlGeoQuery
  extend ActiveModel::Naming
  attr_reader :filters, :errors

  def initialize(filters)
    @filters = filters
    @errors = ActiveModel::Errors.new(self)
  end

  module Scopes
    def by_short_url_path(path)
      return self if path.blank?

      where(path: path)
    end

    def by_visit_date_range(start_date, end_date)
      return self if start_date.blank? || end_date.blank?

      where(geolocations: { created_at: Date.parse(start_date).beginning_of_day..Date.parse(end_date).end_of_day })
    end

    def select_columns(select_statement)
      return self if select_statement.blank?

      select(select_statement)
    end

    def group_by(group_by_statement)
      return self if group_by_statement.blank?

      group(group_by_statement)
    end

    def order_by(order_by_statement)
      return self if order_by_statement.blank?

      order(order_by_statement)
    end

  end

  def call
    ShortUrl
      .extending(Scopes)
      .joins(:geolocations)
      .by_short_url_path(@filters[:path])
      .by_visit_date_range(@filters[:start_date], @filters[:end_date])
      .select_columns(@filters[:select])
      .group_by(@filters[:group_by])
      .order_by(@filters[:order_by])
  end

  def valid?
    if !(@filters[:start_date].blank? == @filters[:end_date].blank?)
      errors.add(:start_date,
                 'Start date cannot be blank if end date is present') and return false if filters[:start_date].blank?

      errors.add(:end_date,
                 'End date cannot be blank if start date is present') and return false if filters[:end_date].blank?
    end

    if @filters[:start_date].present? && @filters[:end_date].present?
      errors.add(:end_date,
                 'End date cannot be before the start date') and return false if filters[:end_date] < filters[:start_date]
    end
    true
  end

  def read_attribute_for_validation(attr)
    send(attr)
  end

  def self.human_attribute_name(attr, _options = {})
    attr
  end

  def self.lookup_ancestors
    [self]
  end
end
